import datetime

import mysql.connector

mydb = mysql.connector.connect(
    host="localhost",
    # host="192.168.43.42",
    user="root",
    # password="sa123456",
    # password="root",
    password="Chien0001518",
    database="DTV"
)

mycursor = mydb.cursor(buffered=True)

def update_license_plate_case(case_id, license_plate_number):
    val = (license_plate_number, case_id)
    sql = "UPDATE UNCONFIRMED_CASES SET license_plate = %s WHERE case_id = %s"
    mycursor.execute(sql, val)
    mydb.commit()


def insert_case(violation_id, location, license_plate, image_id):
    created_date = datetime.datetime.now()
    value = [violation_id, location, license_plate, created_date, image_id]
    sql = "INSERT INTO UNCONFIRMED_CASES(violation_id, location, license_plate, created_date, image_id) VALUES (%s, %s, %s, %s, %s)"
    message = 'Successfully inserted THE RED LIGHT PASS case!'
    if violation_id == 2:
        message = 'Successfully inserted CROSSING case!'
    elif violation_id == 3:
        message = 'Successfully inserted THE LINE ENCROACHMENT case!'
    elif violation_id == 4:
        message = 'Successfully inserted THE ABSENCE OF A HELMET!'
    elif violation_id == 5:
        message = 'Successfully inserted THE WRONG LANE case!'
    mycursor.execute(sql, value)
    mydb.commit()
    return message


def update_image(id, url):
    val = (url, id)
    sql = "UPDATE IMAGES SET url = %s WHERE image_id = %s"
    mycursor.execute(sql, val)
    mydb.commit()
