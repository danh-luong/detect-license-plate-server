import base64
import io
import cv2
import threading

from test import Yolo4
from PIL import Image
from flask import Flask, request, flash, redirect
from flask_restful import Api, Resource
from database_access import update_license_plate_case, update_image, insert_case
from firebase import save


app = Flask(__name__)
api = Api(app)

if __name__ == '__main__':
    app.run(debug=True)

model_path = 'lic.h5'
# File anchors cua YOLO
anchors_path = 'yolo4_anchors.txt'
# File danh sach cac class
classes_path = 'custom.names'
score = 0.5
iou = 0.5
model_image_size = (608, 608)
yolo4_model = Yolo4(score, iou, anchors_path, classes_path, model_path)


class UploadImage(Resource):
    def post(self):
        # json_data = request.json
        # file_upload = json_data['image']
        if 'file' not in request.files:
            flash('No file part')
            # return redirect(request.url)
            # print('allo')
        img = request.files['file']
        if img:
            # image_decoded_bytes = base64.b64decode(file_upload)
            # buff_image = io.BytesIO(image_decoded_bytes)
            image = Image.open(img)
            result = yolo4_model.detect_image(
                image, model_image_size=model_image_size)
            return result


class DetectLicence(Resource):
    def post(self):
        json_data = request.get_json()

        case_id = json_data['violation_id']
        location = json_data['location']
        license_plate_location = json_data['license_plate_location']
        image = json_data['image']

        if image:
            image_decoded_bytes = base64.b64decode(image)
            buff_image = io.BytesIO(image_decoded_bytes)
            image = Image.open(buff_image)
            # coords = yolo4_model.detect_image(
            #     image, model_image_size=model_image_size)
            coords = (license_plate_location[0][0], license_plate_location[0][1], license_plate_location[0][2], license_plate_location[0][3])
            result = yolo4_model.detect_licence(image, coords)
            update_license_plate_case(case_id, result)
            return "Success"

    def get(self):
        return {'hello': "from python"}


def insert_firebase(img_id, img_url, file_name):
    try:
        img_url = save(file_name, img_url)
        update_image(img_id, img_url)
        print('Inserted firebase')
    except:
        print('Reinsert firebase')
        insert_firebase(img_id, img_url, file_name)
        pass


class DetectLicences(Resource):
    def post(self):
        json_data = request.get_json()
        violation_id = json_data['violation_id']
        location = json_data['location']
        images = json_data['images']
        img_url = json_data['firebase_img']
        img_id = json_data['img_id']
        file_name = json_data['file_name']
        message = 'Insert case failed!' 
        thread = threading.Thread(target=insert_firebase, args=(img_id, img_url, file_name))
        thread.start()
        if images:
            license_plate = ''
            licenses = []
            numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']
            is_inserted = False
            for path in images:
                image = cv2.imread(path)
                if image is not None:
                    try:
                        result = yolo4_model.detect_licence(image)
                        confidence = 0
                        if len(result) == 8 or len(result) == 9:
                            confidence += 4
                        if len(result) > 2:
                            if str(result[2]) not in numbers:
                                confidence += 2
                        if len(result) > 1:
                            if str(result[1]) not in numbers:
                                confidence += 1 
                        if len(result) > 3:
                            if str(result[3]) not in numbers:
                                confidence += 1 
                        if confidence > 4:
                            tmp_result = ''
                            for character in result:
                                tmp_character = character
                                if character == 'B':
                                    tmp_character = '8'
                                elif character == 'L':
                                    tmp_character = '1'
                                elif character == 'G':
                                    tmp_character = '6'
                                tmp_result += tmp_character
                            final = ''
                            count = 0
                            for character in tmp_result:
                                tmp_character = character
                                count += 1
                                if len(tmp_result) >= 3 and count == 3:
                                    if tmp_result[2] == '8':
                                        tmp_character = 'B'
                                    elif tmp_result[2] == '6':
                                        tmp_character = 'G'
                                    elif tmp_result[2] == '1':
                                        tmp_character = 'L'
                                    elif tmp_result[2] == '7':
                                        tmp_character = 'L'
                                final += tmp_character
                            message = insert_case(violation_id, location, final, img_id)
                            print(str(final) + ' (Last license)')
                            is_inserted = True
                            break
                        else:
                            licenses.append([result, confidence])
                    except:
                        print('Fail to detect')
                        pass
            if len(licenses) > 0 and is_inserted == False:
                index = 0          
                max_text = licenses[(0)][0]
                for i in range(1, len(licenses)):
                    if len(max_text) < len(licenses[i][0]):
                        index = i
                        max_text = licenses[i][0]
                licenses[index][1] = licenses[index][1] + 3
                index = 0
                max_confidence = licenses[(0)][1]
                for i in range(1, len(licenses)):
                    if max_confidence < licenses[i][1]:
                        index = i
                        max_confidence = licenses[i][1]
                tmp_last = licenses[index][0]
                last = ''
                for character in tmp_last:
                    tmp_character = character
                    if character == 'B':
                        tmp_character = '8'
                    elif character == 'L':
                        tmp_character = '1'
                    elif character == 'G':
                        tmp_character = '6'
                    last += tmp_character
                final = ''
                count = 0
                for character in last:
                    tmp_character = character
                    count += 1
                    if len(last) >= 3 and count == 3:
                        if last[2] == '8':
                            tmp_character = 'B'
                        elif last[2] == '6':
                            tmp_character = 'G'
                        elif last[2] == '1':
                            tmp_character = 'L'
                        elif last[2] == '7':
                            tmp_character = 'L'
                    final += tmp_character
                print(str(final) + ' (Last license)')
                message = insert_case(violation_id, location, final, img_id)
        thread.join()
        return message


api.add_resource(UploadImage, "/detect_image")
api.add_resource(DetectLicence, "/licence")
api.add_resource(DetectLicences, "/licences")
